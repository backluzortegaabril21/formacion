package net.techu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TiendaApplication {
    public static void main(String[] arg){
        SpringApplication.run(TiendaApplication.class, arg);

    }
}
