package net.techu;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ProductosV2Controller {

    private ArrayList<Productos> listaProductos = null;

    public ProductosV2Controller() {
        listaProductos = new ArrayList<>();
        listaProductos.add(new Productos(1, "PR1",27.35));
        listaProductos.add(new Productos(2, "PR2", 18.33));
    }

    /* Get lista de productos */
    @GetMapping(value = "/v2/productos", produces = "application/json")
    public ResponseEntity<List<Productos>> obtenerListado()
    {
        System.out.println("Estoy en obtener");
        return new ResponseEntity<>(listaProductos, HttpStatus.OK);
    }

    @GetMapping("/v2/productos/{id}")
    public ResponseEntity<Productos> obtenerProductoPorId(@PathVariable int id)
    {
        Productos resultado = null;
        ResponseEntity<Productos> respuesta = null;
        try
        {
            resultado = listaProductos.get(id);
            respuesta = new ResponseEntity<>(resultado, HttpStatus.OK);
        }
        catch (Exception ex)
        {
            //String mensaje = "No se ha encontrado el producto";
            //respuesta = new ResponseEntity(mensaje, HttpStatus.NOT_FOUND);
            respuesta = new ResponseEntity(resultado, HttpStatus.NOT_FOUND);
        }
        return respuesta;
    }

    /* Add Nuevo Producto*/
    @PostMapping(value = "/v2/productos", produces="application/json")
    public ResponseEntity<String> addProducto(@RequestBody Productos productoNuevo) {
        System.out.println("Estoy en añadir");
        System.out.println(productoNuevo.getId());
        System.out.println(productoNuevo.getNombre());
        System.out.println(productoNuevo.getPrecio());
        //listaProductos.add(new Producto(99, nombre, 100.5));
        listaProductos.add(productoNuevo);
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }


    /* Add nuevo producto con nombre */
    @PostMapping(value = "/v2/productos/{nom}/{cat}", produces="application/json")
    public ResponseEntity<String> addProductoConNombre(@PathVariable("nom") String nombre, @PathVariable("cat") String categoria) {
        System.out.println("Voy a añadir el producto con nombre " + nombre + " y categoría " + categoria);
        listaProductos.add(new Productos(99, "NUEVO", 100.5));
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }

    @PutMapping("/v2/productos/{id}")
    public ResponseEntity<String> updateProducto(@PathVariable int id, @RequestBody Productos cambios)
    {
        ResponseEntity<String> resultado = null;
        try
        {
            Productos productoAModificar = listaProductos.get(id);
            System.out.println("Voy a Modificar el producto");
            System.out.println("Precio actual:" + String.valueOf(productoAModificar.getPrecio()));
            System.out.println("Precio nuevo:" + String.valueOf(productoAModificar.getPrecio()));
            productoAModificar.setNombre(cambios.getNombre());
            productoAModificar.setPrecio(cambios.getPrecio());
            listaProductos.set(id, productoAModificar);
            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex)
        {
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;
    }
    /* Modifica Todo */
    @PutMapping("/v2/productos")
    public ResponseEntity<String> subirPrecio()
    {
        ResponseEntity<String> resultado = null;
        for (Productos p:listaProductos) {
            p.setPrecio(p.getPrecio()*1.25);
        }
        resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return resultado;
    }


    @DeleteMapping("/v2/productos/{id}")
    public ResponseEntity<String> deleteProducto(@PathVariable int id )
    {
        ResponseEntity<String> resultado = null;
        try
        {
            Productos productoAEliminar = listaProductos.get(id-1);
            System.out.println("Voy a Eliminar el producto");
            listaProductos.remove(id-1);
            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex)
        {
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;
    }

    /* Elimina Todo */
    @DeleteMapping("/v2/productos")
    public ResponseEntity<String> deleteProducto()
    {
        ResponseEntity<String> resultado = null;
        listaProductos.clear();
        //for (Productos p: listaProductos) {
        //    listaProductos.remove(p);
        //}
        resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return resultado;
    }
}
